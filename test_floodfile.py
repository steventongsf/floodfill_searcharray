

import unittest

class Board:

    def __init__(self):
        self.rows = {}
        self.rows["a"] = ["w","b","w","b"]
        self.rows["b"] = ["b", "b", "b", "w"]
        self.rows["c"] = ["w", "b", "w", "w"]
        self.rows["d"] = ["w", "b", "b", "w"]

    def print_values(self):
        print "Begin"
        print self.rows["a"]
        print self.rows["b"]
        print self.rows["c"]
        print self.rows["d"]
        print "End"

    def start_update_adjacent_cells(self,row,col):
        cur_color = self.rows[row][col]
        # change color
        if cur_color == "w":
            new_color = "b"
        else:
            new_color = "w"
        # set first cell
        #self.rows[row][col] = new_color
        self.update_cells(row,col, new_color)

    def update_cells(self, row, col,color):
        cell_cnt = len(self.rows["a"]) - 1
        # test bounds
        if ord(row) < ord("a"):
            return
        if ord(row) > ord("d"):
            return
        if col >= cell_cnt or col < 0:
            return
        # update current cell
        if self.rows[row][col] == color:
            # already the correct color
            return
        else:
            # update the color
            self.rows[row][col] = color
            # get adjacent cells, test for color and update
            # update left
            self.update_cells(row, col - 1, color)
            # update right
            self.update_cells(row, col + 1, color)
            # update up
            self.update_cells(chr(ord(row) - 1), col, color)
            # update down
            self.update_cells(chr(ord(row) + 1), col, color)



class FloodfillTestCase(unittest.TestCase):

    def test_filladjacent1(self):
        board = Board()
        board.start_update_adjacent_cells("d", 0)
        expected = Board()
        expected.rows["a"] = ["w","b","w","b"]
        expected.rows["b"] = ["b","b","b","w"]
        expected.rows["c"] = ["b","b","w","w"]
        expected.rows["d"] = ["b","b","b","w"]
        for s in ["a","b", "c","d"]:
            self.assertListEqual(expected.rows[s], board.rows[s])

    def test_filladjacent2(self):
        board = Board()
        board.start_update_adjacent_cells("b", 1)
        expected = Board()
        expected.rows["a"] = ["w", "w", "w", "b"]
        expected.rows["b"] = ["w", "w", "w", "w"]
        expected.rows["c"] = ["w", "w", "w", "w"]
        expected.rows["d"] = ["w", "w", "w", "w"]
        for s in ["a", "b", "c", "d"]:
            self.assertListEqual(expected.rows[s], board.rows[s])

    def test_filladjacent3(self):
        board = Board()
        board.start_update_adjacent_cells("a", 3)
        expected = Board()
        expected.rows["a"] = ["w", "b", "w", "w"]
        expected.rows["b"] = ["b", "b", "b", "w"]
        expected.rows["c"] = ["w", "b", "w", "w"]
        expected.rows["d"] = ["w", "b", "b", "w"]
        for s in ["a", "b", "c", "d"]:
            self.assertListEqual(expected.rows[s], board.rows[s])

