import unittest

class SearchArrayTestCase(unittest.TestCase):

    def find_num1(self,array, num):
        for n in array:
            if n == num:
                return num
    def find_num2(self,array, num):
        if num >= array[len(array) - 1]:
            # start from 0
            for n in range(0, len(array) - 1):
                if num == array[n]:
                    return num
        else:
            # work backwards
            for n in range(len(array) - 1, 0, -1):
                if num == array[n]:
                    return num

    def test_find_num1(self):
        arr1 = [3,4,5,6,1,2]
        self.assertEqual(6, self.find_num1(arr1, 6))

    def test_find_num2(self):
        arr1 = [3,4,5,6,1,2]
        self.assertEqual(6, self.find_num2(arr1, 6))
